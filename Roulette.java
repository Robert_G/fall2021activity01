import java.util.Scanner;
public class Roulette 
{
    public static void main(String[] args)
    {
        Scanner reader = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        boolean escape = false;
        int won = 0;
        int lost = 0;
        while(escape == false)
        {
            System.out.println("How much you'd like to bet?");
            int bet = reader.nextInt();
            System.out.println("Would you'd like to bet?");
            String answer = reader.next();
            
            if (answer.equals("yes"))
            {
                System.out.println("What number you'd like to bet on? Must be between 0 to 36");
                int number = reader.nextInt();
                // System.out.println("Want to bet on Highs & Lows?");
                // String answer2 = reader.next();
                // if(answer2.equals("yes"))
                // {
                //     System.out.println("Which one? High or Low?");
                //     String highLowAnswer = reader.next();
                //     int betHighLow = bet();
                // }
                // System.out.println("Want to bet on Even & Odd?");
                // String answer3 = reader.next();
                // if(answer3.equals("yes"))
                // {
                //     System.out.println("Which one? Even or Odd?");
                //     String oddEvenAnswer = reader.next();
                //     int betOddEven = bet();
                // }
                // System.out.println("Want to bet on Black & Red?");
                // String answer4 = reader.next();
                // if(answer4.equals("yes"))
                // {
                //     System.out.println("Which one? Black or Red?");
                //     String blackRedAnswer = reader.next();
                //     int betBlackRed = bet();
                // }
                wheel.spin();
                System.out.println("It lands on "+wheel.getValue()+"\n");
                System.out.println("Low? "+wheel.isLow()+"\nHigh? "+wheel.isHigh()+"\nEven? "+wheel.isEven()+"\nOdd? "+wheel.isOdd()+"\nRed? "+wheel.isRed()+"\nBlack? "+wheel.isBlack());
                if(wheel.getValue() == number)
                {
                    System.out.println("It rolled to the right number! You won: $"+bet*35);
                    won += bet*35;
                }
                else
                {
                    System.out.println("Aww. It didn't land on your number. You lost "+bet+".");
                    lost += bet;
                }
            }
            else 
            {
                System.out.println("Ending game...\nResult are:\nAmount Won: "+won+"\nAmount Lost: "+lost+"");
                escape = true;
            }
        }
    }
    public static int bet()
    {
        Scanner reader = new Scanner(System.in);
        System.out.println("How much you'd want to bet on that?");
        int bet = reader.nextInt();
        return bet;
    }
}
