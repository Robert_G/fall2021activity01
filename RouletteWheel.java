import java.util.Random;
public class RouletteWheel
{
    private Random rand;
    private int lastSpin;

    public RouletteWheel()
    {
        this.rand = new Random();
        this.lastSpin = 0;
    }
    public void spin()
    {
        this.lastSpin = rand.nextInt(37);
    }
    public int getValue()
    {
        return this.lastSpin;
    }
    public boolean isLow()
    {
        if(this.lastSpin >= 1 && this.lastSpin <= 18)
        {
            return true;
        }
        return false;
    }
    public boolean isHigh()
    {
        if(this.lastSpin >= 19 && this.lastSpin <= 36)
        {
            return true;
        }
        return false;
    }
    public boolean isOdd()
    {
        if(this.lastSpin % 2 != 0)
        {
            return true;
        }
        return false;
    }
    public boolean isEven()
    {
        if(this.lastSpin == 0)
        {
            return false;
        }
        if(this.lastSpin % 2 == 0)
        {
            return true;
        }
        return false;
    }
    public boolean isRed()
    {
        if(this.lastSpin >= 1 && this.lastSpin <= 10 || this.lastSpin >= 19 && this.lastSpin <= 28)
        {
            if(this.isOdd() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (this.lastSpin >= 11 && this.lastSpin <= 18 || this.lastSpin >= 29 && this.lastSpin <= 36)
        {
            if(this.isEven() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
           throw new IllegalArgumentException("Invalid Input.");
        }
    }
    public boolean isBlack()
    {
        if(this.isRed() == false)
        {
            return true;
        }
        return false;
    }
}